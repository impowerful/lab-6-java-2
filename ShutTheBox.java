public class ShutTheBox
{
    public static void main(String[]args)
    {
      System.out.println("Welcome to the Game of Shut The Box");  

      Board board = new Board();
      boolean gameOver = false;

      while(!gameOver)
      {
        System.out.println("\nPlayer 1's Turn");
        System.out.println(board.toString());
     
        if(board.playATurn())
        {
            System.out.println("\nPlayer 2 has Won the Game");
            gameOver = true;
        }
        else
        {
            System.out.println("\nPlayer 2's Turn");
            System.out.println(board.toString());

            if(board.playATurn())
            {
                System.out.println("\nPlayer 1 has Won the Game");
                gameOver = true;
            }
        } 
      }
    }
}