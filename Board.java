public class Board
{
    private Die DieOne;
    private Die DieTwo;
    private boolean[] closedTiles;

    public Board()
    {
        this.DieOne = new Die();
        this.DieTwo = new Die();
        this.closedTiles = new boolean[12];
    }
    public String toString()
    {
        String check = " ";

        for(int i = 0; i<closedTiles.length; i++)
        {
            if(closedTiles[i])
            {
                check += "X ";
            }
            else
            {
                check += (i+1) + " ";
            }
        }
        return check;
    }
    public boolean playATurn()
    {
        DieOne.roll();
        DieTwo.roll();

        System.out.println(DieOne.toString());
        System.out.println(DieTwo.toString());
    
        int sum = DieOne.getPips() + DieTwo.getPips();
        boolean isClosed = false;

        if(!closedTiles[sum-1])
        {
            closedTiles[sum-1] = true;

            System.out.println("Closing Tile: " + sum);
        }
        else
        {
            System.out.println("This Tile is Already Shut");

            isClosed = true;
        }
        
        return isClosed;
    }
}